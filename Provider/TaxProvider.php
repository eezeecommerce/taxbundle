<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 10:08
 */

namespace eezeecommerce\TaxBundle\Provider;


use eezeecommerce\SettingsBundle\Provider\SettingsProvider;

class TaxProvider
{
    /**
     * @var SettingsProvider
     */
    private $settings;

    public function __construct(SettingsProvider $settings)
    {
        $this->settings = $settings->loadSettingsBySite();
    }

    public function loadTaxSettings()
    {
        return $this->settings->getTaxSettings();
    }
}