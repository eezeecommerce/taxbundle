<?php

namespace eezeecommerce\TaxBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class TaxRatesType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rate')
            ->add('tax_settings', "entity", array(
                    "label" => false,
                    'class' => "eezeecommerceSettingsBundle:TaxesSettings",
                    'property' => 'settings.store_name',
                    'multiple' => false,
                    'expanded' => true,
                    'required' => true
                ))
            ->add('country', 'entity', array(
                    'class' => "eezeecommerceShippingBundle:Country",
                    'query_builder' => function(\Doctrine\ORM\EntityRepository $em) {
                        return $em->createQueryBuilder("c")
                                ->where("c.tax_rate is null");
                    },
                    'property' => "name",
                    "multiple" => true,
                ))
                            ->add('allow_override', null, array(
                                "required" => false,
                            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'eezeecommerce\TaxBundle\Entity\TaxRates'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'eezeecommerce_taxbundle_taxrates';
    }
}
