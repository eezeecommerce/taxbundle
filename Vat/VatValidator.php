<?php

namespace eezeecommerce\TaxBundle\Vat;

use Sparkling\VATBundle\Service\VATService;

class VatValidator
{
    /**
     * @var VATService
     */
    private $service;

    /**
     * VatValidator constructor.
     * @param VATService $service
     */
    public function __construct(VATService $service)
    {
        $this->service = $service;
    }

    public function validate($vatNumber)
    {
        if (null === $vatNumber) {
            return [
                "error" => false,
                "valid" => false
            ];
        }

        $vatNumber = preg_replace("/\s/", "", strtoupper($vatNumber));

        $match = preg_match("/^[A-Z]{2}+[0-9]+/", $vatNumber);

        if (!$match) {
            return [
                "error" => "Your vat number is in an incorrect format. Please try again using the following format: GB123456789",
                "valid" => false
            ];
        }

        $valid = $this->service->validate($vatNumber);

        if (!$valid) {
            return [
                "error" => "You have entered an incorrect vat number.",
                "valid" => false
            ];
        }

        return [
            "error" => false,
            "valid" => true
        ];
    }
}