<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/05/16
 * Time: 13:18
 */

namespace eezeecommerce\TaxBundle\Calculator;


use eezeecommerce\TaxBundle\Entity\TaxRates;


interface CalculatorInterface
{
    /**
     * Calculate the rate of tax
     *
     * @param float    $base Base Price
     * @param TaxRates $rate Tax Rate Entity
     *
     * @return float
     */
    public function calculate($base);
}