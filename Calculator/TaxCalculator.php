<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/05/16
 * Time: 13:20
 */

namespace eezeecommerce\TaxBundle\Calculator;


use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use eezeecommerce\TaxBundle\Entity\TaxRates;

class TaxCalculator implements CalculatorInterface
{

    protected $inclusive;

    protected $rate;

    public function setRate(TaxRates $rate = null)
    {
        $this->inclusive = $rate->getTaxSettings()->getInclusive();
        $this->rate = $rate;
    }

    /**
     * {@inheritdoc}
     */
    public function calculate($base)
    {
        if (null === $this->rate) {
            return 0;
        }

        if ($this->inclusive) {
            return (round(($base - ($base / (1 + ($this->rate->getRate() / 100))))
                * 100)) / 100;
        }

        return (round(($base * ($this->rate->getRate() / 100)) * 100)) / 100;
    }

    public function getInclusive()
    {
        return $this->inclusive;
    }
}