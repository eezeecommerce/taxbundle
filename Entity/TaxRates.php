<?php

/*
 * Developed by EezeeCommerce
 * All rights reserved and subject to copyright.
 * https://www.eezeecommerce.com
 */

namespace eezeecommerce\TaxBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Table(name="tax_rates")
 * @ORM\Entity(repositoryClass="eezeecommerce\TaxBundle\Entity\TaxRatesRepository")
 */
class TaxRates
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     * 
     * @var integer $id
     */
    protected $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="eezeecommerce\SettingsBundle\Entity\TaxesSettings", inversedBy="tax_rates")
     * @ORM\JoinColumn(name="tax_id", referencedColumnName="id")
     */
    protected $tax_settings;
    
    /**
     * @ORM\OneToMany(targetEntity="eezeecommerce\ShippingBundle\Entity\Country", mappedBy="tax_rate", cascade={"persist"})
     */
    protected $country;
    
    /**
     * @ORM\Column(type="boolean")
     */
    protected $allow_override;
    
    /**
     * @ORM\Column(name="rate", type="smallint")
     * @Assert\Range(
     *          min = 0,
     *          max = 100
     * )
     */
    protected $rate;

    
    public function __construct()
    {
        $this->country = new ArrayCollection();
    }
    
    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set taxSettings
     *
     * @param \eezeecommerce\SettingsBundle\Entity\TaxesSettings $taxSettings
     *
     * @return TaxRates
     */
    public function setTaxSettings(\eezeecommerce\SettingsBundle\Entity\TaxesSettings $taxSettings = null)
    {
        $this->tax_settings = $taxSettings;
        
        return $this;
    }

    /**
     * Get taxSettings
     *
     * @return \eezeecommerce\SettingsBundle\Entity\TaxesSettings
     */
    public function getTaxSettings()
    {
        return $this->tax_settings;
    }

    /**
     * Set rate
     *
     * @param integer $rate
     *
     * @return TaxRates
     */
    public function setRate($rate)
    {
        $this->rate = $rate;

        return $this;
    }

    /**
     * Get rate
     *
     * @return integer
     */
    public function getRate()
    {
        return $this->rate;
    }

    /**
     * Add country
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $country
     *
     * @return TaxRates
     */
    public function addCountry(\eezeecommerce\ShippingBundle\Entity\Country $country)
    {
        $this->country[] = $country;
        
        $country->setTaxRate($this);
        
        return $this;
    }
    
    public function setCountry(\eezeecommerce\ShippingBundle\Entity\Country $country)
    {
        $this->country[] = $country;
        
        $country->setTaxRate($this);
        
        return $this;
    }

    /**
     * Remove country
     *
     * @param \eezeecommerce\ShippingBundle\Entity\Country $country
     */
    public function removeCountry(\eezeecommerce\ShippingBundle\Entity\Country $country)
    {
        $this->country->removeElement($country);
    }

    /**
     * Get country
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCountry()
    {
        
        return $this->country;
    }

    /**
     * Set allowOverride
     *
     * @param boolean $allowOverride
     *
     * @return TaxRates
     */
    public function setAllowOverride($allowOverride)
    {
        $this->allow_override = $allowOverride;

        return $this;
    }

    /**
     * Get allowOverride
     *
     * @return boolean
     */
    public function getAllowOverride()
    {
        return $this->allow_override;
    }
}
