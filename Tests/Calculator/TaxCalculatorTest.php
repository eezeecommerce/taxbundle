<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 06/05/16
 * Time: 13:28
 */

namespace eezeecommerce\TaxBundle\Tests\Calculator;


use eezeecommerce\SettingsBundle\Entity\TaxesSettings;
use eezeecommerce\TaxBundle\Calculator\TaxCalculator;
use eezeecommerce\TaxBundle\Entity\TaxRates;

class TaxCalculatorTest extends \PHPUnit_Framework_TestCase
{

    public function testCalculatorImplementsCalculatorInterface()
    {
        $calc = new \ReflectionClass(TaxCalculator::class);

        $this->assertTrue($calc->implementsInterface("eezeecommerce\TaxBundle\Calculator\CalculatorInterface"));
    }

    public function testCalculatorReturnsCorrectInclusiveTax()
    {
        $taxSettingsInc = $this->getMockBuilder(TaxesSettings::class)
            ->disableOriginalConstructor()
            ->getMock();
        $taxSettingsInc->expects($this->any())
            ->method("getInclusive")
            ->will($this->returnValue(true));


        $rates = $this->getMockBuilder(TaxRates::class)
            ->disableOriginalConstructor()
            ->getMock();

        $rates->expects($this->any())
            ->method("getRate")
            ->will($this->returnValue(20));

        $rates->expects($this->any())
            ->method("getTaxSettings")
            ->will($this->returnValue($taxSettingsInc));

        $amount = 1;

        $calc = new TaxCalculator();
        $actual = $calc->calculate($amount, $rates);

        $this->assertEquals(0.17, $actual);

        $actual = $calc->calculate(138.93, $rates);

        $this->assertEquals(23.16, $actual);
    }

    public function testCalculatorReturnsCorrectTaxForExclusivePrice()
    {
        $taxSettingsInc = $this->getMockBuilder(TaxesSettings::class)
            ->disableOriginalConstructor()
            ->getMock();
        $taxSettingsInc->expects($this->any())
            ->method("getInclusive")
            ->will($this->returnValue(false));


        $rates = $this->getMockBuilder(TaxRates::class)
            ->disableOriginalConstructor()
            ->getMock();

        $rates->expects($this->any())
            ->method("getRate")
            ->will($this->returnValue(20));

        $rates->expects($this->any())
            ->method("getTaxSettings")
            ->will($this->returnValue($taxSettingsInc));

        $amount = 1;

        $calc = new TaxCalculator();
        $actual = $calc->calculate($amount, $rates);

        $this->assertEquals(0.20, $actual);

        $actual = $calc->calculate(138.93, $rates);

        $this->assertEquals(27.79, $actual);
    }

    public function testCalculatorWithFifteenPercent()
    {
        $taxSettingsInc = $this->getMockBuilder(TaxesSettings::class)
            ->disableOriginalConstructor()
            ->getMock();
        $taxSettingsInc->expects($this->any())
            ->method("getInclusive")
            ->will($this->returnValue(false));


        $rates = $this->getMockBuilder(TaxRates::class)
            ->disableOriginalConstructor()
            ->getMock();

        $rates->expects($this->any())
            ->method("getRate")
            ->will($this->returnValue(15));

        $rates->expects($this->any())
            ->method("getTaxSettings")
            ->will($this->returnValue($taxSettingsInc));

        $amount = 1;

        $calc = new TaxCalculator();
        $actual = $calc->calculate($amount, $rates);

        $this->assertEquals(0.15, $actual);

        $actual = $calc->calculate(138.93, $rates);

        $this->assertEquals(20.84, $actual);
    }

    public function testCalculatorWithZero()
    {
        $taxSettingsInc = $this->getMockBuilder(TaxesSettings::class)
            ->disableOriginalConstructor()
            ->getMock();
        $taxSettingsInc->expects($this->any())
            ->method("getInclusive")
            ->will($this->returnValue(false));


        $rates = $this->getMockBuilder(TaxRates::class)
            ->disableOriginalConstructor()
            ->getMock();

        $rates->expects($this->any())
            ->method("getRate")
            ->will($this->returnValue(20));

        $rates->expects($this->any())
            ->method("getTaxSettings")
            ->will($this->returnValue($taxSettingsInc));

        $amount = 0;

        $calc = new TaxCalculator();
        $actual = $calc->calculate($amount, $rates);

        $this->assertEquals(0, $actual);
    }

    public function testNullTaxRateReturnsZero()
    {
        $amount = 14;
        $calc = new TaxCalculator();
        $actual = $calc->calculate($amount);

        $this->assertEquals(0, $actual);
    }


}