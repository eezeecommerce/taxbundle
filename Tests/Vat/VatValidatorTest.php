<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25/05/16
 * Time: 10:20
 */

namespace eezeecommerce\TaxBundle\Tests\Vat;


use eezeecommerce\TaxBundle\Vat\VatValidator;
use Sparkling\VATBundle\Service\VATService;

class VatValidatorTest extends \PHPUnit_Framework_TestCase
{
    public function testConstructorTakesOneArg()
    {
        $mock = $this->getMockBuilder(VATService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator = new VatValidator($mock);

        $this->assertNotFalse($validator);
    }

    public function testValidatorFailsOnInvalidVatNumber()
    {
        $mock = $this->getMockBuilder(VATService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator = new VatValidator($mock);

        $result = $validator->validate("123");

        $expected = "Your vat number is in an incorrect format. Please try again using the following format: GB123456789";

        $this->assertArrayHasKey("error", $result);
        $this->assertArrayHasKey("valid", $result);
        $this->assertFalse($result["valid"]);
        $this->assertEquals($expected, $result["error"]);
    }

    public function testValidatorFailsOnInvalidButCorrectlyFormated()
    {
        $mock = $this->getMockBuilder(VATService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator = new VatValidator($mock);

        $expected = "You have entered an incorrect vat number.";

        $result = $validator->validate("GB123456789");

        $this->assertArrayHasKey("error", $result);
        $this->assertArrayHasKey("valid", $result);
        $this->assertFalse($result["valid"]);
        $this->assertEquals($expected, $result["error"]);
    }

    public function testValidatorOnValidVatNumber()
    {
        $mock = $this->getMockBuilder(VATService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $mock->expects($this->once())
            ->method("validate")
            ->will($this->returnValue(true));

        $validator = new VatValidator($mock);

        $expected = false;

        $result = $validator->validate("GB842706037");

        $this->assertArrayHasKey("error", $result);
        $this->assertArrayHasKey("valid", $result);
        $this->assertTrue($result["valid"]);
        $this->assertEquals($expected, $result["error"]);
    }

    public function testNullVatNumber()
    {
        $mock = $this->getMockBuilder(VATService::class)
            ->disableOriginalConstructor()
            ->getMock();

        $validator = new VatValidator($mock);

        $result = $validator->validate(null);

        $this->assertArrayHasKey("error", $result);
        $this->assertArrayHasKey("valid", $result);
        $this->assertFalse($result["valid"]);
        $this->assertFalse($result["error"]);
    }
}