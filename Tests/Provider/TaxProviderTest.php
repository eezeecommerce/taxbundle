<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 20/05/16
 * Time: 10:20
 */

namespace eezeecommerce\TaxBundle\Tests\Provider;

use eezeecommerce\SettingsBundle\Provider\SettingsProvider;
use eezeecommerce\SettingsBundle\Entity\Settings;
use eezeecommerce\SettingsBundle\Entity\TaxesSettings;
use eezeecommerce\TaxBundle\Provider\TaxProvider;

class TaxProviderTest extends \PHPUnit_Framework_TestCase
{
    public function testTaxProviderProvidesValidTaxSettings()
    {
        $taxSettings = $this->getMockBuilder(TaxesSettings::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settings = $this->getMockBuilder(Settings::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settings->expects($this->once())
            ->method("getTaxSettings")
            ->will($this->returnValue($taxSettings));

        $settingsProvider = $this->getMockBuilder(SettingsProvider::class)
            ->disableOriginalConstructor()
            ->getMock();

        $settingsProvider->expects($this->once())
            ->method("loadSettingsBySite")
            ->will($this->returnValue($settings));

        $taxProvider = new TaxProvider($settingsProvider);

        $this->assertEquals($taxSettings, $taxProvider->loadTaxSettings());
    }
}